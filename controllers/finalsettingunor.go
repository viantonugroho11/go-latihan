package controllers

import
(
		"go-latihan/models"
    "net/http"
    

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)	
type CreateFinalInput struct
{
	Id string `json:"id"`
	Instansi_id string `json:"instansi_id"`
	Instansi_nama string `json:"instansi_nama"`
	Step_unor_verifikasi_approval int `json:"step_unor_verifikasi_approval"`
	Step_relasi_unor int `json:"step_relasi_unor"`
	Status_final_setting int `json:"status_final_setting"`
}

type UpdateFinalInput struct
{
	Id string `json:"id"`
	Instansi_id string `json:"instansi_id"`
	Instansi_nama string `json:"instansi_nama"`
	Step_unor_verifikasi_approval int `json:"step_unor_verifikasi_approval"`
	Step_relasi_unor int `json:"step_relasi_unor"`
	Status_final_setting int `json:"status_final_setting"`
}


func AllFinals(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var final []models.FinalSettingUnor
    db.Find(&final)

    c.JSON(http.StatusOK, gin.H{"data": final})
}

// POST /final
// Create new final
func CreateFinal(c *gin.Context) {
    // Validate input
    var input CreateFinalInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create final
    final := models.FinalSettingUnor{
			Id :input.Id,
			Instansi_id :input.Instansi_id,
			Instansi_nama :input.Instansi_nama,
			Step_unor_verifikasi_approval : input.Step_unor_verifikasi_approval,
			Step_relasi_unor :input.Step_relasi_unor,
			Status_final_setting : input.Status_final_setting,
		}

    db := c.MustGet("db").(*gorm.DB)
    db.Create(&final)

    c.JSON(http.StatusOK, gin.H{"data": final})
}

// GET /final/:id
// Find a final
func FindFinal(c *gin.Context) { // Get model if exist
    var final models.FinalSettingUnor

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&final).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK , gin.H{"data": final})
}

// PATCH /final/:id
// Update a final
func UpdateFinal(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var final models.FinalSettingUnor
    if err := db.Where("id = ?", c.Param("id")).First(&final).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Data tidak ditemukan!!"})
        return
    }

    // Validate input
    var input UpdateFinalInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }


    var updatedInput models.FinalSettingUnor
    	updatedInput.Id =input.Id
			updatedInput.Instansi_id =input.Instansi_id
			updatedInput.Instansi_nama =input.Instansi_nama
			updatedInput.Step_unor_verifikasi_approval = input.Step_unor_verifikasi_approval
			updatedInput.Step_relasi_unor =input.Step_relasi_unor
			updatedInput.Status_final_setting = input.Status_final_setting

    db.Model(&final).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": final})
}

// DELETE /final/:id
// Delete a final
func DeleteFinal(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var final models.FinalSettingUnor
    if err := db.Where("id = ?", c.Param("id")).First(&final).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Data tidak ditemukan!!"})
        return
    }

    db.Delete(&final)

    c.JSON(http.StatusOK, gin.H{"data": true})
}