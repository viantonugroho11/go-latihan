package controllers

import (
	// "encoding/json"
	// "encoding/json"
	"fmt"
	"go-latihan/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// type EventSel struct
// {
// id string `json:"id"`,
// instansi_id string `json:"instansi_id"`,
// tgl_awal string `json:"tgl_awal"`,
// }
type CreateEventInput struct
{
	ID string `json:"ID"`
	Event_id string `son:"event_id"`
	Instansi_id string `json:"instansi_id"`
	Instansi_nama string `json:"instansi_nama"`
	Satuan_kerja_id string `json:"satuan_kerja_id"`
	Satuan_kerja_nama string `json:"satuan_kerja_nama"`
	Jenis string `json:"jenis"`
	Jenis_instansi_id string `json:"jenis_instansi_id"`
	Status_aktif bool `json:"status_aktif"`
	Kanreg_id string `json:"kanreg_id"`
	Kanreg_nama string `json:"kanreg_nama"`
	Periode int `json:"periode"`
	Tgl_awal string `json:"tgl_awal"`
	Tgl_akhir string `json:"tgl_akhir"`
}

type SelectEvent struct
{
	*models.EventInstansi
	ID string `json:"ID"`
	Instansi_id string `json:"instansi_id"`
	Tgl_awal string `json:"tgl_awal"`
	Status bool `json:"status"`
}

type UpdateEventInput struct
{
	ID string `json:"ID"`
	Event_id string `son:"event_id"`
	Instansi_id string `json:"instansi_id"`
	Instansi_nama string `json:"instansi_nama"`
	Satuan_kerja_id string `json:"satuan_kerja_id"`
	Satuan_kerja_nama string `json:"satuan_kerja_nama"`
	Jenis string `json:"jenis"`
	Jenis_instansi_id string `json:"jenis_instansi_id"`
	Status_aktif bool `json:"status_aktif"`
	Kanreg_id string `json:"kanreg_id"`
	Kanreg_nama string `json:"kanreg_nama"`
	Periode int `json:"periode"`
	Tgl_awal string `json:"tgl_awal"`
	Tgl_akhir string `json:"tgl_akhir"`
}

func SelectEvents(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// var event []models.EventInstansi
	// db.Select([]string{"ID", "event_id"}).Find(&event)

	// result := map[string]interface{}{{}}
	var results []map[string]interface{}
	db.Table("event_instansis").Select("ID","instansi_id","tgl_awal").Find(&results)
	// var p []models.EventInstansi
	// db.Model(&models.EventInstansi{}).Find(&SelectEvent{})

	c.JSON(http.StatusOK, gin.H{"data":  results})
}

func AllEventsSelect(c *gin.Context){
db := c.MustGet("db").(*gorm.DB)
    var event []SelectEvent
		
    db.Table("event_instansis").Select("ID","instansi_id","tgl_awal").Find(&event)

    c.JSON(http.StatusOK, gin.H{"data": event})
}

// func ActiveEvent(c *gin.Context){
// 	db := c.MustGet("db").(*gorm.DB)
// 	emp1:=new(SelectEvent)
// 	result := db.Table("event_instansis").Select("ID","instansi_id","tgl_awal")
// 	json.Marshal(result)
// }

func ActiveEvents(c *gin.Context){
	db := c.MustGet("db").(*gorm.DB)
	// enc := json.NewEncoder(os.Stdout)
	// result := []map[string]interface{}{}
	// db.Table("event_instansis").Select("ID","instansi_id","tgl_awal").Find(&result)
	// db.Exec("UPDATE event_instansis SET status_aktif = true WHERE id IN ?", })

	var event []SelectEvent
		
  db.Table("event_instansis").Select("ID","instansi_id","tgl_awal").Find(&event)
	for key := range event {
		if (event[key].Tgl_awal <= time.Now().String()) {
			// fmt.Printf("event[key].Status: %v\n", event[key].Status)
			event[key].Status=false
		}else{
			event[key].Status=true
		}
		fmt.Println("test")
	}
	c.JSON(http.StatusOK, gin.H{"data": event}) 
}

func AllEvents(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var event []models.EventInstansi
		
    db.Find(&event)

    c.JSON(http.StatusOK, gin.H{"data": event})
}

// POST /event
// Create new event
func CreateEvent(c *gin.Context) {
    // Validate input
    var input CreateEventInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

		date := "2006-01-02"
    tgl_Awal, _ := time.Parse(date, input.Tgl_awal)
    tgl_Akhir, _ := time.Parse(date, input.Tgl_akhir)

    // Create event
    event := models.EventInstansi{
			ID : input.ID,
			Event_id : input.Event_id,
			Instansi_id :input.Instansi_id,
			Instansi_nama :input.Instansi_nama,
			Satuan_kerja_id :input.Satuan_kerja_id,
			Satuan_kerja_nama :input.Satuan_kerja_nama,
			Jenis :input.Jenis,
			Jenis_instansi_id :input.Jenis_instansi_id,
			Status_aktif :input.Status_aktif,
			Kanreg_id :input.Kanreg_id,
			Kanreg_nama :input.Kanreg_nama,
			Periode :input.Periode,
			Tgl_awal: tgl_Awal,
			Tgl_akhir: tgl_Akhir,
		}

    db := c.MustGet("db").(*gorm.DB)
    db.Create(&event)

    c.JSON(http.StatusOK, gin.H{"data": event})
}

// GET /event/:id
// Find a event
func FindEvent(c *gin.Context) { // Get model if exist
    var event models.EventInstansi

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&event).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK , gin.H{"data": event})
}

// PATCH /event/:id
// Update a event
func UpdateEvent(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var event models.EventInstansi
    if err := db.Where("id = ?", c.Param("id")).First(&event).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Data tidak ditemukan!!"})
        return
    }

    // Validate input
    var input UpdateEventInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    date := "2006-01-02"
    tgl_Awal, _ := time.Parse(date, input.Tgl_awal)
    tgl_Akhir, _ := time.Parse(date, input.Tgl_akhir)

    var updatedInput models.EventInstansi
    	updatedInput.ID = input.ID
			updatedInput.Event_id = input.Event_id
			updatedInput.Instansi_id = input.Instansi_id
			updatedInput.Instansi_nama = input.Instansi_nama
			updatedInput.Satuan_kerja_id = input.Satuan_kerja_id
			updatedInput.Satuan_kerja_nama = input.Satuan_kerja_nama
			updatedInput.Jenis = input.Jenis
			updatedInput.Jenis_instansi_id = input.Jenis_instansi_id
			updatedInput.Status_aktif = input.Status_aktif
			updatedInput.Kanreg_id = input.Kanreg_id
			updatedInput.Kanreg_nama = input.Kanreg_nama
			updatedInput.Periode = input.Periode
			updatedInput.Tgl_awal = tgl_Awal
			updatedInput.Tgl_akhir = tgl_Akhir

    db.Model(&event).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": event})
}

// DELETE /event/:id
// Delete a event
func DeleteEvent(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var event models.EventInstansi
    if err := db.Where("id = ?", c.Param("id")).First(&event).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Data tidak ditemukan!!"})
        return
    }

    db.Delete(&event)

    c.JSON(http.StatusOK, gin.H{"data": true})
}