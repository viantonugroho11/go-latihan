package models
import
(
	"time"
)
type FinalSettingUnor struct{
		Id string `gorm:"size:255;primary_key;not null" json:"id"`
		Instansi_id string `gorm:"size:255" json:"instansi_id"`
		Instansi_nama string `gorm:"size:255" json:"instansi_nama"`
		Step_unor_verifikasi_approval int `json:"step_unor_verifikasi_approval"`
		Step_relasi_unor int `json:"step_relasi_unor"`
		Status_final_setting int `json:"status_final_setting"`
		Created_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"create_at"`
		Updated_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"update_at"`
		Deleted_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"deleted_at"`
}