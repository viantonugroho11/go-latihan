package models

import
(
	"time"
)

type EventInstansi struct{
  ID string `gorm:"size:255" json:"ID"`
	Event_id string `gorm:"size:255" json:"event_id"`
	Instansi_id string `gorm:"size:255" json:"instansi_id"`
	Instansi_nama string `gorm:"size:255" json:"instansi_nama"`
	Satuan_kerja_id string `gorm:"size:255" json:"satuan_kerja_id"`
	Satuan_kerja_nama string `gorm:"size:255" json:"satuan_kerja_nama"`
	Jenis string `gorm:"size:10" json:"jenis"`
	Jenis_instansi_id string `gorm:"size:255" json:"jenis_instansi_id"`
	Status_aktif bool `json:"status_aktif"`
	Kanreg_id string `gorm:"size:255" json:"kanreg_id"`
	Kanreg_nama string `gorm:"size:255" json:"kanreg_nama"`
	Created_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	Updated_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Deleted_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"deleted_at"`
	Periode int `json:"periode"`
	Tgl_mulai_verifikasi time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"tgl_mulai_verifikasi"`
	Tgl_akhir_verifikasi time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"tgl_akhir_verifikasi"`
	Tgl_mulai_approval time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"tgl_mulai_approval"`
	Tgl_akhir_approval time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"tgl_akhir_approval"`
	Tgl_awal time.Time `json:"tgl_awal"`
	Tgl_akhir time.Time `json:"tgl_akhir"`
}