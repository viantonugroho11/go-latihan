package routes

import
(
	"go-latihan/controllers"

  "github.com/gin-gonic/gin"
  "gorm.io/gorm"
)

func SetupRoutes(db *gorm.DB) *gin.Engine {
    r := gin.Default()
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })
    r.GET("/event", controllers.AllEvents)
    r.GET("/events", controllers.SelectEvents)
    r.GET("/eventss", controllers.ActiveEvents)
    r.GET("/eventsss", controllers.AllEventsSelect)
    r.POST("/event", controllers.CreateEvent)
    r.GET("/event/:id", controllers.FindEvent)
    r.PATCH("/event/:id", controllers.UpdateEvent)
    r.DELETE("event/:id", controllers.DeleteEvent)

    r.GET("/final", controllers.AllFinals)
    r.POST("/final", controllers.CreateFinal)
    r.GET("/final/:id", controllers.FindFinal)
    r.PATCH("/final/:id", controllers.UpdateFinal)
    r.DELETE("final/:id", controllers.DeleteFinal)
    return r
}