package main

import
(
	"go-latihan/config"
	"go-latihan/models"
	"go-latihan/routes"
)
func main() {
	db := config.SetupDB()
	db.AutoMigrate(&models.EventInstansi{})
	db.AutoMigrate(&models.FinalSettingUnor{})
	r := routes.SetupRoutes(db)
  r.Run()
	
}